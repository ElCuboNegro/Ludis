---
tags:
  - Language server
  - Server
  - HTTP SERVER
---

# Language Server

This package contains the Language server.

It is a web server responsible for hosting domain agnostic NLP models. These includes SentencePiece and FastText models to tokenize and vectorize text.

## Licensing

This software is protected by the same license as the [main ludis repository](https://github.com/ludis/ludis). You can find the license file [here](https://github.com/ludis/ludis/blob/master/LICENSE).
