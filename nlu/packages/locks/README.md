---
tags:
  - NLU packages
  - Locks
  - NLU
---

# ludis Locks

Contains locks and transaction queues to prevent many kinds of race conditions

Race conditions can occur:

- in single-threaded apps when using asynchronous code
- in multi-threaded/multi-process apps
- in mutli-instances apps

## Licensing

This software is protected by the same license as the [main ludis repository](https://github.com/ludis/ludis). You can find the license file [here](https://github.com/ludis/ludis/blob/master/LICENSE).
