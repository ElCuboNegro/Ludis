---
tags:
  - NLU packages
  - Engine
  - NLU
---

# Ludis NLU Engine

## Description

Contains all ludis NLU Pipeline and tools packaged under very few classes and functions.

The `makeEngine()` function returns an instance of class `Engine` which is reponsible for:

- #training of a #model
- #prediction using a #model
- spawning and handling training process
- spawning and handling training threads
- keeping loaded models in an in-memory cache
- handling file-system caches at provided path
- #comunicating via #HTTP with #language_server and #duckling_server

It is not responsible for:

- models persistency
- training state persistency
- hosting an HTTP API
- HA / multi-clustering

The nlu engine has:
- [assets](./assets/README): a set of #pre_trained_models and #stop_words
- [source](source): the code of the engine itself, it has inside
	- [engine](NLU_ENGINES_EXPLANATION.md):
	- [language-service](./src/language-service/README):
	- [ml](MACHINE%20LEARNING%20ALGORITHMS%20TOOLS.md)

