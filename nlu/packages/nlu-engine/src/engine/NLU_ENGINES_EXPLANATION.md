---
tags:
- NLU
- NLU-engine
- Algorithms
---
		
The NLU engine handles
	-  [utterances](./utterances)

From where extracts:
	- [entities](./entities)
	- [intents](./intents)
	- [language](./language)
	- [slots](./slots)

and has:
	- [test-utils](./test-utils)
	- [tools](./tools)
	- [training-process-pool](training-process-pool)