import { makeProcessor } from '@ludis/node-sentencepiece'
import { MLToolkit } from '../typings'

export const processor: () => Promise<MLToolkit.SentencePiece.Processor> = () => {
  return makeProcessor()
}
