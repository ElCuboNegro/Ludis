---
tags:
  - NLU packages
  - SVM
  - NLU
  - Algorithms
---

the libsvm has two algorithms:
[grid search](./grid-search/readme)
[kfold](./kfold/readme)
