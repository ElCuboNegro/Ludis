---
tags:
- NLU
- ML
- Algorithms
---

here are the following ML algorithms:
[crf](./crf/readme)
[fasttext](nlu/packages/node-fasttext/fasttext/fasttext)
[ml-thread-pool](./ml-thread-pool/readme)
[sentencepiece](nlu/packages/node-sentencepiece/sentencepiece/SentencePiece)
[svm](nlu/packages/nlu-engine/src/ml/svm/libsvm/libsvm)