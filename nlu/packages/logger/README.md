---
tags:
  - NLU packages
  - Logger
  - NLU
---

# NLU Logger

This package contains a Logger for NLU web servers and other products.

## Licensing

This software is protected by the same license as the [main ludis repository](https://github.com/ludis/ludis). You can find the license file [here](https://github.com/ludis/ludis/blob/master/LICENSE).
