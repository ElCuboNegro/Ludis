# ludis NLU

<img src="./readme.gif"/>

## Description

This repo contains every ML/NLU related code written by botpress in the NodeJS environment.

The source code is structured in a mono-repo fashion using yarn workspaces. The `./packages` directory contains all available packages. The main packages are:

- [litfan](litfan.md):  Ludis Independent Testing Framework for Algorithms of NLU
- [e2e](E2E.md):  End to end testing
- [lang-server](LUDIS%20LANGUAGE%20SERVER.md): Contains the ludis Language Server
- [locks](nlu/packages/locks/README.md): Contains locks and transaction queues to prevent many kinds of race conditions
- [logger](nlu/packages/logger/README.md): Contains the logger
- [nlu-cli](NLU%20CLI.md): Small CLI to use as an entry point for both `nlu-server` and `lang-server`
- [nlu-server](./packages/nlu-server/readme.md): Contains the ludis Standalone NLU Server
- [nlu-engine](LUDIS_NLU_ENGINE.md): Contains all ludis NLU Pipeline and tools packaged under very few classes and functions.
- [node-crfsuite](./packages/node-crfsuite/README): CRF or Conditional Random Fields are a class of statistical modeling method often applied in pattern recognition and machine learning and used for structured prediction.
- [node-fasttext](NODE_FASTTEXT.md): A Library for efficient text classification and representation learning.
- [node-sentencepiece](./packages/node-sentencepiece/README): an unsupervised text tokenizer and detokenizer mainly for Neural Network-based text generation systems where the vocabulary size is predetermined prior to the neural model training.
- [node-svm](./packages/node-svm/README): This package is a nodejs binding for [Libsvm](https://github.com/cjlin1/libsvm).
- [worker](./packages/worker/README): tools for process and thread pools.



Check out each individual packages for more details.

## Running from source

**Prerequisites**: Node 12.13 (you can use [nvm](https://github.com/creationix/nvm)) and Yarn.

1. Run `yarn` to fetch node packages.
1. Run `yarn build && yarn start` to build and start the Standalone NLU server.
1. You can also run `yarn dev` to run the NLU Server with [ts-node](https://github.com/TypeStrong/ts-node) however, trainings won't be parallelized on several threads.

## Running from pre-built binaries

New executable binary files are packaged at every release. You can download those directly on release page located [here](https://github.com/ludis/nlu/releases).

## ⚠️⚠️ Disclaimer ⚠️⚠️

The NLU Server does **not** enforce authentication in any way. This means it is completely exposed to many attacks. If you plan on using the nlu-server in your local ludis setup, makes sure it is not publicly exposed. If you plan on exposing the NLU server, make sure it his hidden behind a reverse proxy which ensures a proper authentication. This reverse proxy should:

- Ensure each appId (`X-App-Id` header) is unique.
- Ensure a user with appId `user1` can't call the nlu server with header `X-App-Id` set to anything other than `user1`.
- Ensure only calls with a registered appId can call the nlu server except maybe for the `GET /info` route.

The NLU Server's only purpose is to do NLU.

## Licensing

Different liscences may apply to differents packages of the [./packages](https://github.com/ludis/nlu/tree/master/packages) directory. If no liscence is specified, the package is protected by the same license as the [main ludis repository](https://github.com/ludis/ludis). You can find the license file [here](/LICENSE) and the changelog [here](CHANGELOG) .
