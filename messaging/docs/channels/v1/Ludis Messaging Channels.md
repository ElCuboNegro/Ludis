---
tags:
- messaging_channels
---

# ludis Messaging Channels Version 1.0.0

- [Changelog](./changelog.md)

## Channels

- [Facebook Messenger](./messenger.md)
- [Slack](./slack.md)
- [Smooch](./smooch.md)
- [Teams](./teams.md)
- [Telegram](./telegram.md)
- [Twilio](./twilio.md)
- [Vonage](./vonage.md)

#Todo: implement instagram, email, sms among others

## Development

To make change to any of the supported channels, please follow the documentation [here](messaging/packages/channels/LUDIS%20MESSAGING%20CHANNELS.md)

