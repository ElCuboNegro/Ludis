import { uuid } from '@ludis/messaging-base'

export interface Client {
  id: uuid
}
