import { StudioConnector } from './typings'

declare global {
  export interface Window {
    ludis?: StudioConnector
  }
}
