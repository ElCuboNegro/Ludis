import { MessagingSocket } from '@ludis/messaging-socket'

declare global {
  interface Window {
    websocket: MessagingSocket
  }
}

declare module '*.scss'
