import { uuid } from '@ludis/messaging-base'

export interface House {
  id: uuid
  clientId: uuid
  address: string
}
