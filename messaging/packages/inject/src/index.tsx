import { ExposedWebChat } from '@ludis/webchat'
import React from 'react'
import ReactDOM from 'react-dom'

ReactDOM.render(<ExposedWebChat fullscreen={false} />, document.getElementById('app'))
