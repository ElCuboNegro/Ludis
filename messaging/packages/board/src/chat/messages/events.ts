import { Emitter, Message } from '@ludis/messaging-base'

export enum MessagesEvents {
  Receive = 'receive',
  Send = 'send'
}

export class MessagesEmitter extends Emitter<{
  [MessagesEvents.Receive]: Message[]
  [MessagesEvents.Send]: any
}> {}

export type MessagesWatcher = Omit<MessagesEmitter, 'emit'>
