import { uuid } from '@ludis/messaging-base'

export interface Conduit {
  id: uuid
  providerId: uuid
  channelId: uuid
  config: any
}
