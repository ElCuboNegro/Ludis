import { Message, uuid } from '@ludis/messaging-base'
import { Dispatcher } from '@ludis/messaging-engine'

export enum ConverseDispatches {
  Message,
  Stop
}

export interface ConverseMessageDispatch {
  message: Message
}

export interface ConverseStopDispatch {
  conversationId: uuid
}

export class ConverseDispatcher extends Dispatcher<{
  [ConverseDispatches.Message]: ConverseMessageDispatch
  [ConverseDispatches.Stop]: ConverseStopDispatch
}> {}
