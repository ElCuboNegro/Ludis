import { uuid } from '@ludis/messaging-base'

export interface Provision {
  clientId: uuid
  providerId: uuid
}
