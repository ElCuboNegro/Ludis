import { Dispatcher } from '@ludis/messaging-engine'

export enum InstanceLifetimeDispatches {
  Stop
}

export interface InstanceLifetimeStopDispatch {}

export class InstanceLifetimeDispatcher extends Dispatcher<{
  [InstanceLifetimeDispatches.Stop]: InstanceLifetimeStopDispatch
}> {}
