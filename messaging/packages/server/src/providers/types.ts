import { uuid } from '@ludis/messaging-base'

export interface Provider {
  id: uuid
  name: string
  sandbox: boolean
}
