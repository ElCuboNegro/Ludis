import { uuid } from '@ludis/messaging-base'

export interface Sender {
  id: uuid
  identityId: uuid
  name: string
}
