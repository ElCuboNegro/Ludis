import { uuid } from '@ludis/messaging-base'

export interface Usermap {
  tunnelId: uuid
  userId: uuid
  senderId: uuid
}
