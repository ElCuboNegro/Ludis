import { uuid } from '@ludis/messaging-base'

export interface Thread {
  id: uuid
  senderId: uuid
  name: string
}
