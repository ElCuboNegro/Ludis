import { uuid } from '@ludis/messaging-base'

export interface Identity {
  id: uuid
  tunnelId: uuid
  name: string
}
