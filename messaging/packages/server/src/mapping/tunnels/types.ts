import { uuid } from '@ludis/messaging-base'

export interface Tunnel {
  id: uuid
  clientId: uuid
  channelId?: uuid
  customChannelName?: string
}
