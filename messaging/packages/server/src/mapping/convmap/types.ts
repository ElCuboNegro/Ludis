import { uuid } from '@ludis/messaging-base'

export interface Convmap {
  tunnelId: uuid
  conversationId: uuid
  threadId: uuid
}
