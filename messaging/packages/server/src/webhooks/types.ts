import { uuid } from '@ludis/messaging-base'

export interface Webhook {
  id: uuid
  clientId: uuid
  url: string
  token: string
}
