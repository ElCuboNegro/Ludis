---
tags:
  - ROOT
  - LUDIS
---

# Ludis

[![Contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg)](CONTRIBUTING.md)
[![License](https://img.shields.io/badge/License-Apache%202.0-brightgreen.svg)](https://opensource.org/licenses/Apache-2.0)


Ludis is _the_ engine we use to handle all the communications with users, from our frontend chatbot to our HITL, all runs here :)

The project has 4 main parts:
1. [The duckling server](DUCKLING.md), provided by Facebook
1. [The messaging server](LUDIS_MESSAGING.md), based on the botpress messaging server
3. [The NLU server](LUDIS%20NLU.md), based on the NLU server
5. [The Studio](./studio/STUDIO), based on the botpress Studio