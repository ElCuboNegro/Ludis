import { Button, Icon, Popover, PopoverInteractionKind, PopoverPosition } from '@blueprintjs/core'
import { NodeTransition } from 'ludis/sdk'
import { lang } from 'ludis/shared'
import { FlowView } from 'common/typings'
import _ from 'lodash'
import React, { Component, Fragment } from 'react'

import TransitionItem from '../common/TransitionItem'

import style from './style.scss'
import TransitionModalForm from './TransitionModalForm'

interface Props {
  items: NodeTransition[]
  readOnly: boolean
  canPaste: boolean
  currentFlow: FlowView
  subflows: string[]
  onItemsUpdated: (items: NodeTransition[]) => void
  copyItem: (item: NodeTransition) => void
  pasteItem: () => void
  currentNodeName?: string
}

interface State {
  itemToEditIndex: number | null
  showConditionalModalForm: boolean
}

export default class TransitionSection extends Component<Props, State> {
  state: State = {
    itemToEditIndex: null,
    showConditionalModalForm: false
  }

  onMove(prevIndex: number, direction: number) {
    const clone = [...this.props.items]
    const a = clone[prevIndex]
    const b = clone[prevIndex + direction]

    clone[prevIndex + direction] = a
    clone[prevIndex] = b

    this.props.onItemsUpdated(clone)
  }

  onSubmit = (item: NodeTransition) => {
    const editIndex = this.state.itemToEditIndex
    const { items } = this.props
    const updateByIndex = (originalItem, i) => (i === editIndex ? item : originalItem)
    this.setState({ showConditionalModalForm: false, itemToEditIndex: null })
    this.props.onItemsUpdated(Number.isInteger(editIndex) ? items.map(updateByIndex) : [...items, item])
  }

  onRemove(index: number) {
    const clone = [...this.props.items]
    _.pullAt(clone, [index])
    this.props.onItemsUpdated(clone)
  }

  onCopyAction(index: number) {
    this.props.copyItem(this.props.items[index])
  }

  onEdit(itemToEditIndex: number) {
    this.setState({ itemToEditIndex, showConditionalModalForm: true })
  }

  render() {
    const { items = [], readOnly } = this.props
    const handleAddAction = () => this.setState({ showConditionalModalForm: true })

    return (
      <Fragment>
        <div id="transition-section">
          {items.map((item, i) => (
            <Popover
              wrapperTagName="div"
              targetProps={{ id: `transition-item-${i}` }}
              interactionKind={PopoverInteractionKind.HOVER}
              position={PopoverPosition.BOTTOM}
              key={`${i}.${item}`}
            >
              <TransitionItem className={style.item} transition={item} position={i} displayType />
              {!readOnly && (
                <div className={style.actions}>
                  <a id="transition-edit" onClick={() => this.onEdit(i)}>
                    {lang.tr('edit')}
                  </a>
                  <a id="transition-remove" onClick={() => this.onRemove(i)}>
                    {lang.tr('remove')}
                  </a>
                  <a id="transition-copy" onClick={() => this.onCopyAction(i)}>
                    {lang.tr('copy')}
                  </a>
                  {i > 0 && (
                    <a id="transition-up" onClick={() => this.onMove(i, -1)}>
                      {lang.tr('up')}
                    </a>
                  )}
                  {i < items.length - 1 && (
                    <a id="transition-down" onClick={() => this.onMove(i, 1)}>
                      {lang.tr('down')}
                    </a>
                  )}
                </div>
              )}
            </Popover>
          ))}
          {!readOnly && (
            <div className={style.actions}>
              <Button
                id="btn-add-element"
                minimal
                large
                onClick={handleAddAction}
                icon={<Icon iconSize={16} icon="add" />}
              />
              <Button
                id="btn-paste-element"
                minimal
                large
                onClick={this.props.pasteItem}
                disabled={!this.props.canPaste}
                icon={<Icon iconSize={16} icon="clipboard" />}
              />
            </div>
          )}
        </div>
        {!readOnly && (
          <TransitionModalForm
            currentFlow={this.props.currentFlow}
            currentNodeName={this.props.currentNodeName || ''}
            subflows={this.props.subflows}
            show={this.state.showConditionalModalForm}
            onClose={() => this.setState({ showConditionalModalForm: false, itemToEditIndex: null })}
            onSubmit={this.onSubmit}
            item={items[this.state.itemToEditIndex]}
          />
        )}
      </Fragment>
    )
  }
}
